#ifndef TRAINTICKET_H
#define TRAINTICKET_H

#include <QWidget>
#include <QCheckBox>
#include <QToolBar>
#include <QTableWidget>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCalendarWidget>
#include <QResizeEvent>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrlQuery>
#include "jlog.h"
#include "jlineedit.h"
#include <deque>
#include <iostream>
#include <QRegExp>

using namespace std;


// 车次信息结构体
typedef struct TrainInfo {
    QString TrainNo;        // 车次
    QString StartCode;      // 始发站编码
    QString StartName;      // 始发站站名
    QString EndCode;        // 终点站编码
    QString EndName;        // 终点站站名
    QString StartTime;      // 开车时间
    QString EndTime;        // 到站时间
    QString SpendTime;      // 历时时长
    QString IsOrder;        // 是否可以预定
    // 席位
    QString GRSleep;        // 高级软卧
    QString OtherSeat;      // 其他座
    QString RSleep;         // 软卧
    QString RSeat;          // 软座
    QString SSeat;          // 特等座
    QString Stand;          // 无座
    QString DSleep;         // 动卧
    QString HSleep;         // 硬卧
    QString HSeat;          // 硬座
    QString SecSeat;        // 二等座
    QString FirstSeat;      // 一等座
    QString BusSeat;        // 商务座
}TRAININFO;

// 站名信息结构体
typedef struct StationName {
    QString Acronym;        // 站名首字母缩写
    QString Spelling;       // 站名拼音
    QString NameZN;         // 中文站名
    QString Code;           // 站名代码
}STATIONNAME;

namespace Ui {
class TrainTicket;
}

class TrainTicket : public QWidget
{
    Q_OBJECT

public:
    explicit TrainTicket(QWidget *parent = 0);
    ~TrainTicket();

private slots:
    void resizeEvent(QResizeEvent *e);

    void LogInOutSlot();

    void ReceiveLogInfo(QString Usr, QString Pwd);

    void LeftTicketClick();

    void ReceiveNetworkInfo(QNetworkReply *Info);

    void CalenderWidget();

    void ReceiveCalenDate();

    void AllSeatClick();

    void SubDateClick();

    void AddDateClick();

    // 车站站名查找
    void CheckStartStationName(QString);

    void CheckEndStationName(QString);

    void OrderTicketsSlot();

private:
    Ui::TrainTicket *ui;

    // 变量
    QToolBar *ToolBar;
    QAction *LogInOut;
    bool IsLog;
    QVBoxLayout *MainWin;
    JLog *Log;
    QString User;
    QString Passwd;
    QNetworkAccessManager * Manage;    
    QStringList TicketHeader;
    deque<TrainInfo> TicketsList;
    QMap<QString, QString> StationMap;
    QMap<QString, QString> CodeNameMap;
    deque<StationName> StationList;
    // 界面控件
    QLabel * LbSrc;
    QComboBox * cBoxSrc;        // 始发站名
    QPushButton * pBtnExchange; // 站点交换
    QLabel *LbDest;
    QComboBox *cBoxDest;        // 终点站名
    QLabel *LbDate;
    QPushButton *pBtnSubDate;   // 减日期
    JLineEdit *LEdtDate;
    QCalendarWidget *Calend;    // 日期
    bool IsCalenShow;
    QPushButton *pBtnAddDate;   // 加日期
    QSpacerItem *HSpStation;
    QHBoxLayout *HLayInfo;
    QLabel *LbSeat;
    QCheckBox *kBoxAllSeat;
    QCheckBox *kBoxBussiness;   // 商务座
    QCheckBox *kBoxFrist;       // 一等座
    QCheckBox *kBoxSecond;      // 二等座
    QCheckBox *kBoxHSoftSeat;   // 高级软卧
    QCheckBox *kBoxSoftPer;     // 软卧
    QCheckBox *kBoxDSoftper;    // 动卧
    QCheckBox *kBoxYPer;        // 硬卧
    QCheckBox *kBoxSoftSeat;    // 软座
    QCheckBox *kBoxSeat;        // 硬座
    QCheckBox *kBoxStand;       // 无座
    QCheckBox *kBoxOther;       // 其他
    QHBoxLayout *HLaySeat;      // 布局器
    QVBoxLayout *VLayInfo;
    QFrame * LineSep;
    QCheckBox *kBoxAdult;
    QCheckBox *kBoxStudent;
    QCheckBox *kBoxChild;
    QLineEdit *LEdtChildCnt;
    QPushButton *pBtnLeftTickets;
    QGridLayout *GLayQuery;
    QHBoxLayout *HLayOperat;
    QTableWidget *TableTrainInfo;
    deque<QPushButton *> pBtnListOrder;
    QVBoxLayout *VLayTrainInfo;
    QLabel *LbStaticInfo;           // 提示信息
    QLabel *LbRTInfo;               // 动态提示信息
    QWidget *TabRobTicket;

    // 初始化函数
    void Init();
    // 初始化工具栏
    void InitToolBar();
    // 初始化控件
    void InitCtrls();
    // 初始化车次信息表
    void InitTrainTable();
    // 选中席位
    void IsSeatCheck(bool Check);
    // 解析车次信息
    void ParseTicketInfo(QString Info);
    // 解析站点信息
    void ParseStationInfo(QString Info);
    // 显示余票信息
    void DisplayTicketInfo();
    // 查找车站
    int GetStationNameList(deque<StationName> &List, const QString Reg);
    // 预定前检查是否登录
    bool ParseIsLogin(QString Info);
};

#endif // TRAINTICKET_H
