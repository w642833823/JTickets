#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T19:05:41
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JTicket
TEMPLATE = app


SOURCES += main.cpp\
        trainticket.cpp \
    jlog.cpp \
    jlineedit.cpp

HEADERS  += trainticket.h \
    jlog.h \
    jlineedit.h

FORMS    += trainticket.ui \
    jlog.ui

RESOURCES += \
    res.qrc
